import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

public class GUIMenu {

	private static String[] menuItem = { "Multiply 2 Numbers",
			"Divide 2 Numbers", "Add 2 Numbers", "Subtract 2 Numbers" };

	private static void init() {
		// Add code for initialization here

	}

	private static void doOption(int choice) {
		switch (choice) {
		case 0:
			GUIMenu.output("MULTIPLY TWO NUMBERS");
            int a = GUIKeyboard.readInt("Enter 1st number");
            int b = GUIKeyboard.readInt("Enter 2nd number");
            GUIMenu.output(a + " * " + b + " = " + (a * b));
            break;
			// Add codes here
			// GUIConsole.display("This is Option 1");
			
		case 1:
			GUIMenu.output("DIVIDE TWO NUMBERS");
            int c = GUIKeyboard.readInt("Enter 1st number");
            int d = GUIKeyboard.readInt("Enter 2nd number");
            if (d!=0) {
            	GUIMenu.output(c + " � " + d + " = " + (c / d));	
            }
            else {
            	GUIMenu.output("CANNOT DIVIDE BY ZERO");
            	
            }
            
            break;
			// Add codes here
			// GUIConsole.display("This is Option 2");

		case 2:
			GUIMenu.output("ADD 2 NUMBERS");
			int f = GUIKeyboard.readInt("Enter 1st number");
			int g = GUIKeyboard.readInt("Enter 2nd number");
			GUIMenu.output(f + " + " + g + " = " + (f + g));	
			// Add codes here
			// GUIConsole.display("This is Option 3");

			break;
		case 3:
			GUIMenu.output("SUBTRACT 2 NUMBERS");
			int h = GUIKeyboard.readInt("Enter 1st number");
			int i = GUIKeyboard.readInt("Enter 2nd number");
			if (h>i){
				GUIMenu.output(h + " - " + i + " = " + (h - i));
			}
			else {
				GUIMenu.output(i + " - " + h + " = " + (i - h));
			}
			// Add codes here
			// GUIConsole.display("This is Option 4");

			break;
		}
	}

	//
	// DO NOT CHANGE ANY CODE FROM THIS POINT ONWARDS
	// UNLESS YOU UNDERSTAND WHAT YOU ARE DOING
	//
	private static JFrame win;
	private static DefaultListModel listModel = new DefaultListModel();
	private static JList jlist = new JList(listModel);

	public static void showMenu(String title) {
		win = new JFrame(title);
		win.setBounds(100, 100, 600, 400);

		// Create the Menu Option Buttons
		JPanel p1 = new JPanel(new GridLayout(menuItem.length, 1, 5, 5));
		p1.setBorder(new TitledBorder("Menu Items"));
		for (int i = 0; i < menuItem.length; i++) {
			JButton btn = new JButton(menuItem[i]);
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String command = e.getActionCommand();
					for (int i = 0; i < menuItem.length; i++) {
						if (command.equals(menuItem[i])) {
							doOption(i);
							break;
						}
					}
				}
			});
			p1.add(btn);
		}
		win.add(p1, BorderLayout.NORTH);

		// Create the Display Area
		JPanel p2 = new JPanel(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(jlist);
		p2.setBorder(new TitledBorder("Display Area"));
		p2.add(scrollPane, BorderLayout.CENTER);
		jlist.setFont(new Font("Courier New", Font.PLAIN, 14));

		JPanel p3 = new JPanel(new FlowLayout());
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				listModel.clear();
			}
		});

		p3.add(btnClear);
		p2.add(p3, BorderLayout.SOUTH);
		win.add(p2, BorderLayout.CENTER);

		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);

		init();
	}

	public static void output(String line) {
		String[] lines = line.split("\n");
		for (int i = 0; i < lines.length; i++) {
			listModel.addElement(lines[i]);
		}
		jlist.setSelectedIndex(listModel.size() - 1);
		jlist.ensureIndexIsVisible(listModel.size() - 1);
	}

}
